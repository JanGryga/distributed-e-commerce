import axios from 'axios';
import useRequest from '../../hooks/use-request';
import 'react-toastify/dist/ReactToastify.css';
import Router from 'next/router';

import { useState } from 'react';
import { ToastContainer } from 'react-toastify';

const signup = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { doRequest, errors } = useRequest({
    url: '/api/users/signup',
    method: 'post',
    body: {
      email,
      password,
    },
    onSuccess: () => Router.push('/'),
  });

  const onSubmit = async (event) => {
    event.preventDefault();

    doRequest();
  };

  return (
    <div className='container'>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <h1>Sign Up</h1>
        <div className='form-group'>
          <label>Email Address</label>
          <input
            type='text'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className='form-control'
          />
        </div>
        <div className='form-group'>
          <label>Password</label>
          <input
            type='password'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className='form-control'
          />
        </div>
        <button className='btn btn-primary'>Sign Up</button>
      </form>
    </div>
  );
};

export default signup;
