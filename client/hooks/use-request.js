import axios from 'axios';
import { useState } from 'react';
import { toast } from 'react-toastify';

function useRequest({ url, method, body, onSuccess }) {
  const [errors, setErrors] = useState(null);

  const doRequest = async () => {
    try {
      setErrors(null);
      const response = await axios[method](url, body);

      if (onSuccess) {
        onSuccess(response.data);
      }

      return response.data;
    } catch (err) {
      setErrors(
        err.response.data.errors.map((err) =>
          toast.error(err.message, { position: toast.POSITION.BOTTOM_RIGHT })
        )
      );
    }
  };

  return { doRequest, errors };
}

export default useRequest;
