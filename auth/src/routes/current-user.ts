import express from 'express';
import jwt from 'jsonwebtoken';

import { currentUser } from '@grygstone/common';


const router = express.Router();

router.get('/api/users/currentuser', currentUser, (req,res) => {
  res.send({ currentUser: req.currentUser || null}) // null to not send undefined 
});

export { router as currentUserRouter };