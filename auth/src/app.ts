import express from "express";
import 'express-async-errors';
import { json } from "body-parser";

import cookieSession from 'cookie-session';
import { errorHandler, NotFoundError} from '@grygstone/common';

import { currentUserRouter} from './routes/current-user';
import { signinRouter } from './routes/signin';
import { signupRouter } from './routes/signup';
import { signoutRouter } from './routes/signout';


const app = express();
app.set('trust proxy', true); // Trust nginx - because used 'secure' for  cookies below
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test'
  })
)

app.use(currentUserRouter);
app.use(signinRouter);
app.use(signupRouter);
app.use(signoutRouter);

app.get('*', async (req,res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };