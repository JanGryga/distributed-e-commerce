import axios from 'axios';
import useRequest from '../../hooks/use-request';
import 'react-toastify/dist/ReactToastify.css';
import Router from 'next/router';

import { useState } from 'react';
import { ToastContainer } from 'react-toastify';

const signin = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { doRequest, errors } = useRequest({
    url: '/api/users/signin',
    method: 'post',
    body: {
      email,
      password,
    },
    onSuccess: () => Router.push('/'),
  });

  const onSubmit = async (event) => {
    event.preventDefault();

    doRequest();
  };

  return (
    <div className='container'>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <h1>Sign In</h1>
        <div className='form-group'>
          <label>Email Address</label>
          <input
            type='text'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className='form-control'
          />
        </div>
        <div className='form-group'>
          <label>Password</label>
          <input
            type='password'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className='form-control'
          />
        </div>
        <button className='btn btn-primary'>Sign In</button>
      </form>
    </div>
  );
};

export default signin;
